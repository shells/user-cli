def twofactor_google_enable
	puts "\nIf you've not run the 'google-authenticator' command before, do so now to set up your tokens.".colorize(:green)
	# Command takes no arguments
	@args = "none"
end

def twofactor_google_disable
  puts "\nYou will now have to remove or rename your ~/.google_authenticator file.".colorize(:green)
	# Command takes no arguments
	@args = "none"
end
