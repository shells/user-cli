def contact_email
  client = Mysql2::Client.new(
    :host     => @config['user_db_host'],
    :username => @config['user_db_user'],
    :password => @config['user_db_pass'],
    :database => @config['user_db_db']
  )

  res = client.query("SELECT `email` FROM invites WHERE `username` = '#{@user}' LIMIT 1")
  if res.count > 0
    res.each(:as => :array) do |row|
      print 'Your current email address is: '
      puts "#{row[0]}".colorize(:green)
    end

    puts "Enter new email or press Ctrl+c to cancel."
    print "Email: ".colorize(:yellow)
    newmail = $stdin.gets.chomp

    client.query("UPDATE invites SET `email` = '#{client.escape(newmail)}' WHERE `username` = '#{@user}'")

    puts 'Updated your email address'.colorize(:green)
  else
    puts "Error: Could not find your user. Please contact an admin.".colorize(:red)
  end
  exit
end

def contact_phone
  client = Mysql2::Client.new(
    :host     => @config['user_db_host'],
    :username => @config['user_db_user'],
    :password => @config['user_db_pass'],
    :database => @config['user_db_db']
  )

  res = client.query("SELECT `phone` FROM invites WHERE `username` = '#{@user}' LIMIT 1")
  if res.count > 0
    res.each(:as => :array) do |row|
      print 'Your current phone number is: '
      puts "#{row[0]}".colorize(:green)
    end

    puts "Enter new phone number (including country code) or press Ctrl+c to cancel."
    print "Phone no.: ".colorize(:yellow)
    newphone = $stdin.gets.chomp

    client.query("UPDATE invites SET `phone` = '#{format_phone(client.escape(newphone))}' WHERE `username` = '#{@user}'")

    puts 'Updated your phone number'.colorize(:green)
  else
    puts "Error: Could not find your user. Please contact an admin.".colorize(:red)
  end
  exit
end

def format_phone(number)
  return number.gsub(/^(00|\+)/, '')
end