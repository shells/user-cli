def httpd_vhost_request
	puts "\nEnter domain name you want to add"
	print "Domain: ".colorize(:yellow)
	domain = $stdin.gets.chomp

	puts "\nEnter directory where the site will be"
	puts "or leave blank for your public_html folder"
	print "Directory: ".colorize(:yellow)
	directory = $stdin.gets.chomp
	
	@args = "#{domain} #{directory}"
end

def httpd_public_html_enable
	# Command takes no arguments
	@args = "none"
end

def httpd_public_html_disable
	# Command takes no arguments
	@args = "none"
end

def httpd_logs_enable
	# Command takes no arguments
	@args = "none"
end

def httpd_logs_disable
	# Command takes no arguments
	@args = "none"
end

# Something about making proxy configs