def postgres_request
	puts "\nDo not use this command if you already have one or more postgres databases.".colorize(:red)
	puts "In that case use the 'postgres_request_extra' command instead."
	puts ""
	print "Database name: ".colorize(:yellow)
	@args = $stdin.gets.chomp
end

def postgers_request_extra
	puts "\nONLY use this command if you already have another postgres database".colorize(:red)
	puts "This new database will be available under your existing postgres account"
	puts ""
	print "Additional database name: ".colorize(:yellow)
	@args = $stdin.gets.chomp
end
