def mysql_request
	puts "\nDo not use this command if you already have one or more MySQL databases.".colorize(:red)
	puts "In that case use the 'mysql_request_extra' command instead."
	puts ""
	puts "Database names should only contain: a-z A-Z 0-9 $ _"
	print "Database name: ".colorize(:yellow)
	@args = $stdin.gets.chomp
	mysql_verify_name(@args)
end

def mysql_request_extra
	puts "\nONLY use this command if you already have another MySQL database".colorize(:red)
	puts "This new database will be available under your existing MySQL account"
	puts ""
	puts "Database names should only contain: a-z A-Z 0-9 $ _"
	print "Additional database name: ".colorize(:yellow)
	@args = $stdin.gets.chomp
	mysql_verify_name(@args)
end

def mysql_verify_name(name)
	if( name !~ /^[a-zA-Z0-9_$]+$/ )
			puts ""
			puts " === WARNING! WARNING! WARNING! === ".colorize(:red)
			puts ""
			puts "You picked a database name with illegal characters in it!"
			puts "This means database creation will almost certainly fail."
			puts "Ctrl + C out now and try again unless you are really sure."
			puts ""
	end
end