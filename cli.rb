#!/usr/bin/env ruby
# encoding: utf-8

require 'bunny'
require 'json'
require 'openssl'
require 'time'
require 'colorize'
require 'mysql2'

Dir["/home/cli_tool/user-cli/modules/*.rb"].each {|file| load file.untaint }

@command  = ''
@args     = ''
@user     = ARGV[0] # We count on the SUID wrapper to provide this reliably
@email    = ''
@comments = ''
@config   = {}

def showcommands
    puts "\nAvailable commands:"
    print "  package_install_request   ".colorize(:blue)
    print "[A] ".colorize(:yellow)
    puts "Install a package from the Debian repositories"

    print "  mysql_request             ".colorize(:blue)
    print "[A] ".colorize(:yellow)
    puts "Get a MySQL database"

    print "  mysql_request_extra       ".colorize(:blue)
    print "[A] ".colorize(:yellow)
    puts "Get additional MySQL database"

    print "  postgres_request          ".colorize(:blue)
    print "[A] ".colorize(:yellow)
    puts "Get a Postgres database"

    print "  postgres_request_extra    ".colorize(:blue)
    print "[A] ".colorize(:yellow)
    puts "Get additional Postgres database"

    print "  mongo_request             ".colorize(:blue)
    print "[A] ".colorize(:yellow)
    puts "Get a MongoDB database"

    print "  memory_limit_request      ".colorize(:blue)
    print "[A] ".colorize(:yellow)
    puts "Raise safety cap for memory limit (3.5G -> 11.4G)"

    print "  process_limit_request     ".colorize(:blue)
    print "[A] ".colorize(:yellow)
    puts "Raise satety cap for process limit (75 -> 250)"

    print "  httpd_vhost_request       ".colorize(:blue)
    print "[A] ".colorize(:yellow)
    puts "Use your own domain name"

    print "  httpd_public_html_enable  ".colorize(:blue)
    puts "Enable your public_html directory"

    print "  httpd_public_html_disable ".colorize(:blue)
    puts "Disable your public_html directory"

    print "  httpd_logs_enable         ".colorize(:blue)
    puts "Write apache access & error logs to your home dir"

    print "  httpd_logs_disable        ".colorize(:blue)
    puts "Don't write apache logs"

    print "  contact_email             ".colorize(:blue)
    puts "Update your contact email address"

    print "  contact_phone             ".colorize(:blue)
    puts "Update your contact phone number"

    print "  twofactor_google_enable   ".colorize(:blue)
    puts "Enable Google two-factor authentication"

    print "  twofactor_google_disable  ".colorize(:blue)
    puts "Disable Google two-factor authentication"

    print "  nanobot_setnick           ".colorize(:blue)
    puts "Set extra nickname nanobot will voice in #shells"

    print "  docker_new                ".colorize(:blue)
    print "[N] ".colorize(:red)
    puts "Start a docker container"

    print "  file_backup               ".colorize(:blue)
    print "[N] ".colorize(:red)
    puts "Backup a file or directory"

    print "  file_restore              ".colorize(:blue)
    print "[N] ".colorize(:red)
    puts "Restore a file or directory from backup"

    print "  help                      ".colorize(:blue)
    puts "Show this help"

    print "  exit                      ".colorize(:blue)
    puts "Exit manager"

    print "[A] ".colorize(:yellow)
    puts "= Requires approval by admin"
    print "[N] ".colorize(:red)
    puts "= Not yet implemented"

    getcommand
end

def parsecommand(cmd)
    commands = ['package_install_request',
                'mysql_request', 'mysql_request_extra',
                'postgres_request', 'postgres_request_extra',
                'couch_request', 'couch_request_extra',
                'mongo_request', 'mongo_request_extra',
                'memory_limit_request', 'process_limit_request',
                'httpd_vhost_request',
                'httpd_public_html_enable', 'httpd_public_html_disable',
                'httpd_cgi_bin_enable', 'httpd_cgi_bin_disable',
                'httpd_logs_enable', 'httpd_logs_disable',
                'twofactor_google_enable', 'twofactor_google_disable',
                'nanobot_setnick',
                'docker_new',
                'file_backup', 'file_restore',
                'contact_email', 'contact_phone'
               ]

    if( commands.include? cmd )
        @command = cmd
        send(cmd)
    else
        puts "Not a valid command."
        getcommand
    end
end

def getcommand
    print "\nType '"
    print "help".colorize(:green)
    print "' for commands, '"
    print "exit".colorize(:green)
    print "' to exit.\n"
    print "Command: ".colorize(:yellow)
    cmd = $stdin.gets.chomp

    if(cmd == "help")
        showcommands
    elsif(cmd == "exit" || cmd == "quit")
    exit
    else
        parsecommand(cmd)

        if(@args == '')
            puts 'No input was received.'.colorize(:red)
            getcommand
        end
    end
end

def getemail
    puts "\nEnter email address for status updates."
    print "Email: ".colorize(:yellow)
    @email = $stdin.gets.chomp

    if( @email == "" )
        @email = "#{@user}@insomnia247.nl"
    end
end

def getcomments
    puts "\nEnter any additional info you feel is relevant."
    print "Comments: ".colorize(:yellow)
    @comments = $stdin.gets.chomp
end

def feedback
    puts "\nYour request has been sent."
    print "You will receive status updates on '"
    print "#{@email}".colorize(:green)
    puts "' when your request was processed."
end

options = {}

# Read config
configdata = ''
File.open('/home/cli_tool/user-cli/config/rabbitmq.conf') do |file|
    file.each do |line|
        configdata << line
    end
end

# Parse config
config  = {}
config  = JSON.parse(configdata)
@config = config

getcommand
getemail
getcomments

conn = Bunny.new(
                    :host     => config['host'],
                    :user     => config['user'],
                    :password => config['pass'],
                )
conn.start

ch   = conn.create_channel
tx   = ch.queue(config['channel_send'])

# Read public key so we can decrypt messages
public_key = OpenSSL::PKey::RSA.new(File.read(config['public_keyfile']))

# Build message
message = JSON.generate({
    :server    => config['servername'],
    :command   => @command,
    :args      => @args,
    :user      => @user,
    :email     => @email,
    :timestamp => Time.now.to_i,
    :comments  => @comments
})

# Send request
string = public_key.public_encrypt(message)
ch.default_exchange.publish(string, :routing_key => tx.name)

conn.close

feedback
